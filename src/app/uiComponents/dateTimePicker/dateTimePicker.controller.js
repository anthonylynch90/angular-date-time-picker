angular.module('common')

    .directive('dateTimePicker',["TimeHelper", function(TimeHelper) {
        return {
            restrict: 'EA',
            transclude: true,
            template: '<div ng-transclude></div>'
        };
    }])