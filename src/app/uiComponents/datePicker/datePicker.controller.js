angular.module('common', [])

    .directive('datePicker', function() {
        return {
            restrict: 'EA',
            translude: true,
            scope: {
                'sbBeforeRenderItem': '&?',
            },
            template: '<div class="col-sm-2 date-pick-box">' +
                            '<p class="input-group">' +
                                '<input type="text" class="form-control" datetime-picker="mediumDate" ng-model="datePicked" is-open="calOpen" enable-time="false"  close-on-date-selection="true" datepicker-append-to-body="true" ng-change="sbBeforeRenderItem({date: datePicked})"/>' +
                                '<span class="input-group-btn">' +
                                    '<button type="button" class="btn btn-default" ng-click="calOpen = !calOpen">' +
                                        '<i class="fa fa-calendar"></i>' +
                                    '</button>' +
                                '</span>' +
                            '</p>' +
                        '</div>',
        };
    });