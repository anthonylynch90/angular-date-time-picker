angular.module('common')

    .directive('timePicker', function() {
        return {
            restrict: 'EA',
            translude: true,
            scope: {
                sbBeforeRenderItem: '&?',
                maxTime: '=',
                minTime: '=',
            },
            template: '<div uib-timepicker ng-model="model" ng-change="!sbBeforeRenderItem({time: model})" max="maxTime" min="minTime" show-meridian="false" ng-disabled="!sbBeforeRenderItem({time: model})"></div>',
        };
    });