angular.module('app', ['common', 'ui.bootstrap', 'ui.bootstrap.datetimepicker'])

    .controller('AppController', function($scope, TimeHelper) {
        var isWeekday,
            showTimePicker = false;

        $scope.timeZones = ["America/Los_Angeles", "America/New_York", "America/Phoenix", "America/Denver"]
        $scope.order = {
                    requestedDatetime: null,
                    timeZone: $scope.timeZones[0]
                };

        var maxAndMinTimeSet = function(){
            if(isWeekday){
                $scope.maxTime = TimeHelper.getMaxWeekdayHours();
                $scope.minTime = TimeHelper.getMinWeekdayHours();
            } else {
                $scope.maxTime = TimeHelper.getMaxWeekendHours();
                $scope.minTime = TimeHelper.getMinWeekendHours();
            }
        };

        $scope.timeZoneChanged = function(timeZone) {
            $scope.order.timeZone = timeZone;

            if($scope.order.requestedDatetime !== null){
                TimeHelper.changeTimeZone(moment($scope.order.requestedDatetime), timeZone);
            }
        }

        $scope.beforeRenderTimeItem = function(time){
            $scope.time = time;
            if ($scope.time) {
                $scope.order.requestedDatetime = TimeHelper.combineDateWithTime($scope.date, time, $scope.order.timeZone);
            }
            return showTimePicker;
        }

        $scope.beforeRenderDateItem = function(datePicked){
            var changeCheckIsWeekDay = TimeHelper.isWeekday(datePicked);
            $scope.date = datePicked;
            showTimePicker = true;
            if(changeCheckIsWeekDay !== isWeekday && $scope.time) {
                $scope.time = TimeHelper.checkTimeIsWithinDay(isWeekday, $scope.time);
                $scope.order.requestedDatetime = TimeHelper.combineDateWithTime($scope.date, $scope.time, $scope.order.timeZone);
            } else if ($scope.time) {
                $scope.order.requestedDatetime = TimeHelper.combineDateWithTime($scope.date, $scope.time, $scope.order.timeZone);
            }
            isWeekday = changeCheckIsWeekDay;
            maxAndMinTimeSet();
        }
        maxAndMinTimeSet();
    });