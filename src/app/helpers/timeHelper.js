angular.module('common')

    .factory('TimeHelper', function () {
        var weekdayMinHours = moment("07:00", "HH:mm"),
        weekdayMaxHours = moment("20:00", "HH:mm"),

        weekendMinHours = moment("10:00", "HH:mm"),
        weekendMaxHours = moment("16:00", "HH:mm"),

        checkTimeIsWithinWeekday = function(time) {
            return time.isAfter(weekdayMinHours) && time.isBefore(weekdayMaxHours);
        },

        checkTimeIsWithinWeekend = function(time) {
            return time.isAfter(weekendMinHours) && time.isBefore(weekendMaxHours);
        },

        changeTimeZone = function(date, timeZone){
            var another = date.clone();
            another.tz(timeZone);
            another.add(date.utcOffset() - another.utcOffset(), 'minutes');
            return another.format("MMM Do YYYY hh:mm z");
        },

        combineDateWithTime = function (date, timeDate, timeZone){
            var momentDate = moment(date);
            var momentTime = moment(timeDate);

            return changeTimeZone(momentDate.set({
                                                    'hour' : momentTime.get('hour'),
                                                    'minute'  : momentTime.get('minute'),
                                                    'second' : momentTime.get('second')
                                                 }), timeZone);
        },

        checkIfTimeIsWithinDay = function (isWeekday, timeSelected) {
            var time = moment(moment(timeSelected).format('HH:mm a'), "HH:mm a");
            if (isWeekday) {
                return checkTimeIsWithinWeekday(time);
            } else {
                return checkTimeIsWithinWeekend(time)
            }
        },

        checkTimeIsWithinDay = function(isWeekday, time){
            var isWithinTime = checkIfTimeIsWithinDay(isWeekday, time)
            if (!isWithinTime){
                return null;
            }
            return time;
        }

        return {
            getMaxWeekdayHours: function () {
                return weekdayMaxHours;
            },
            getMinWeekdayHours: function () {
                return weekdayMinHours;
            },
            getMaxWeekendHours: function () {
                return weekendMaxHours;
            },
            getMinWeekendHours: function () {
                return weekendMinHours;
            },
            isWeekday: function (date) {
                var dayNumber = moment(date).day();
                return (dayNumber !== 6 && dayNumber !== 0);
            },
            checkIfTimeIsWithinDay: checkIfTimeIsWithinDay,
            combineDateWithTime: combineDateWithTime,
            changeTimeZone: changeTimeZone,
            checkTimeIsWithinDay: checkTimeIsWithinDay
        }
    });